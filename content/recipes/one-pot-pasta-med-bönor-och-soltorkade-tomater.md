---
title: One pot pasta med bönor och soltorkade tomater
source: https://www.arla.se/recept/vegopasta-med-bonor-och-soltorkad-tomat/
servings: 4
---
## Ingredienser
- 3 Vitlöksklyftor
- 2 msk rapsolja
- 4½ dl Vatten
- 3 dl vispgrädde
- 1 burk Krossade tomater à 400 g
- 300 g Pasta penne, koktid 11 min
- 1 tsk Salt
- ½ tsk Chili flakes
- 1 dl Strimlade soltorkade tomater i olja
- 1 burk Vita bönor à 400 g
- 1 krm Svartpeppar

### Till servering:
- 1 kruka Färsk basilika
- 1 dl Riven Sveciaost
- Finrivet skal av 1 Citron

## Instruktioner
- Skala och skiva vitlöken tunt. Fräs den mjuk i smör-&rapsolja i en stor kastrull.

- Tillsätt vatten, grädde, krossade tomater, pasta, salt, chili flakes och soltorkade tomater. Koka utan lock ca 10 min. Rör om då och då så att pastan inte fastnar i botten.

- Häll av och skölj bönorna. Rör ner i grytan. Låt koka ytterligare ca 2 min tills pastan är al dente. Smaka av med salt och peppar.

- Toppa med basilikablad och riven ost. Riv över citronskal.
