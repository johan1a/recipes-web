---
title: Vegetariska birria tacos
source: https://edgyveggie.se/recept/birria-seitan-taco/
servings: 4
---
## Ingredienser

### Buljong

- 5 dl grönsaksbuljong
- 4 msk chipotlepaste (chipotle i adobo)
- 2 msk vitvinsvinäger
- 2 st gul lök
- 4 st vitlöksklyfta

### Tacofyllning

- 1 pkt/250g Edgy Veggie Seitantaco
- 1 st silverlök
- 1 kruka koriander
- 1 pkt riven mozzarella
- 1 pkt små majstortilla

## Instruktioner

### Buljong

- Hacka lök och vitlök grovt, stek mjuka i cirka 5 minuter.
- Slå på chipotlepaste, vinäger och buljong.
- Låt reducera i cirka 30 minuter och värmehåll till servering.

### Tacofyllning

- Stek seitantaco krispig på medelhög värme i cirka 5 minuter.
- grovhacka koriander och silverlök.
- Värm en stekpanna och lägg ner majstortillan.
- Fyll med ost, seitantaco, lök och koriander.
- Vik ihop och låt steka tills osten smällt. Upprepa till all fyllning är slut.
- Servera birria tacon tillsammans med buljongen och mer hackad koriander och silverlök. Doppa taco i buljongen och njut av en ny fredagsfavorit!
