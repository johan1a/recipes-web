---
title: Overnight oats med fyra sorters topping
source: https://www.ica.se/recept/overnight-oats-med-fyra-sorters-topping-725226/
servings: 1.5
---
## Ingredienser

### Overnight oats
- 1.5 dl Havregryn
- 1.5 dl Havre-, nöt- eller kokosdryck (alternativt mjölk)
- 1.5 tsk Chiafrön
- 0.75 dl Kvarg (smaksatt om du önskar)

### Smaksättning 1: Blåbär, vanilj & agavesirap
- 0.75 dl Blåbär
- 0.375 krm Stött kardemumma
- 0.75 krm Vaniljpulver

#### Topping
- Agavesirap
- Färska bär
- Flagad mandel eller frön

### Smaksättning 2: Hallon, lime & kokos
- 0.75 dl Hallon
- Skal från 0.75 Lime
- 0.75 tsk Kokossocker

#### Topping
- Exotisk frukt
- Kokosflakes
- Kanel

### Smaksättning 3: Kardemumma, kanel, vanilj & äpple
- 0.375 krm Stött kardemumma
- 0.75 tsk Malen kanel
- 0.75 krm Vaniljpulver
- 0.75 st Tärnat äpple

#### Topping
- Flytande honung
- Hackad mandel eller pumpakärnor

### Smaksättning 4: Banan & jordnötssmör
- 0.75 st Mogen mosad banan
- 0.75 msk Jordnötssmör

#### Topping
- Salta jordnötter
- Skivad banan
- Färska blåbär

## Instruktioner

### Overnight oats
- Blanda alla ingredienser i en behållare tillsammans med någon av smaksättningarna.
- Välj gärna kokosdryck till smaksättning nr 2: Hallon, lime & kokos.
- Förslut behållaren väl och låt vila i kylen över natten.

### Topping
- På morgonen toppar du med valfri topping enligt förslagen ovan.

Njut av din overnight oats! 😊
