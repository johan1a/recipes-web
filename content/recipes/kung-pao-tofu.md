---
title: Kung Pao Tofu
source: https://javligtgott.se/recept/kung-pao-tofu/
servings: 4
---
4 portioner

## Ingredienser
### Marinad till tofun
- Kinesisk mörk sojasås 	2 tsk
- Risvinsvinäger 	1 msk
- Fast naturell tofu 	400 g
- Stjälkar selleri 	4 st
- Röda paprikor 	2 st

### Såsen
- Kinesisk vinäger Chinkiang, finns i asiatiska matbutiker på kinesiska hyllan. Eller byt ut mot balsamvinäger 	2 msk
- Japansk sojasås 	1.5 msk
- Hoisinsås 	1 msk
- Sesamolja 	1 tsk
- Socker 	1 msk
- Majsstärkelse 	2 tsk
- Vatten 	0.5 dl
- Sichuanpeppar 	2 tsk
- Rapsolja 	1 msk
- Färska röda chilifrukter eller 10-15 torkade chilis 	2 st
- Salladslökar 	3 st
- Vitlöksklyftor 	2 st
- X2 cm färsk ingefära 	2 st
- Torrostade jordnötter 	1.5 dl

### Till servering
- Jasminris efter smak

## Instruktioner

- Vira in tofun i hushållspapper och lägg i press i 30 min så den vätskar ur en del.
- Rosta dina sichuanfrön och mortla dem därefter till ett fint pulver.
- Skär tofun och paprikan i ca 1,5×1,5 cm stora tärningar, skiva sellerin i ca 0,5 cm tjocka skivor och lägg tillsammans med ingredienserna till marinaden i en plastpåse och låt marinera 30 min.
- Blanda under tiden ingredienserna till såsen: blanda ihop balsamvinäger, sojasåserna, hoisinsås, sesamolja, socker, majsstärkelse och sichuanpeppar. Vispa tills sockret och majsstärkelsen löst upp sig och ställ åt sidan.
- Skala och riv vitlöken och ingefäran. Skiva den färska chilin (ta bort kärnorna om du inte vill att rätten ska bli för stark). Skiva salladslöken och dela upp de vita och de gröna bitarna för sig.
- Värm upp en stekpanna eller wok till hög värme och häll ner lite rapsolja.
- Stek tofun, sellerin och paprikan tillsammans med marinaden i pannan tills den börjar få färg.
- Tillsätt chilin, de vita bitarna av salladslöken, ingefäran och vitlöken snabbt så det börjar dofta gott och löken blir mjuk.
- Häll i såsen och rör om så den täcker grönsakerna och tofun. Rör ner jordnötterna, låt koka upp och stek i 1-2 minuter.
- Toppa med de gröna salladslöksbitarna.
- Klart!
- Servera med klibbigt jasminris.
