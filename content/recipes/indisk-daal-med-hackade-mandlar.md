---
title: Indisk daal med hackade mandlar
source: https://www.mathem.se/recept/indisk-daal-med-hackade-mandlar
servings: 4
---
4 portioner

## Ingredienser

### Daal

 - 1 st Gul lök
 - 1 st Chilipeppar Färsk
 - 1 st Lime
 - 200 g Röda Linser Torkade
 - 100 g Körsbärstomater
 - 130 g Färsk bladspenat
 - 6 dl Vatten
 - 2 dl Kokosmjölk
 - 1 msk Färsk ingefära
 - 1 msk Rapsolja
 - 2 msk Grönsaksbuljong
 - 1 tsk Garam Masala
 - 1 tsk Torkad gurkmeja
 - 1 tsk Salt Fint
 - 2 krm Salt Fint
 - 2 krm Svartpeppar Malen
 - 2 st klyftor Vitlök Vanlig

### Topping

 - 4 msk Sötmandel Naturell
 - 4 msk Matlagningsyoghurt Turkisk
 - 1 st krukor Koriander - färsk

## Tillagning

1. Skala och finhacka lök, vitlök och ingefära. Skölj och finhacka chili.

2. Hetta upp olja i en kastrull och fräs lök, vitlök, ingefära och chili med garam masala och gurkmeja. Tillsätt röda linser, hela körsbärstomater, vatten, kokosmjölk, grönsaksbuljong och salt. Koka upp och låt grytan sjuda på låg värme i ca 15-20 min. Rör om med jämna mellanrum så att inte linserna bränner vid.

3. Tillsätt spenat i grytan och smaka av med salt, peppar och limesaft.

4. Topping: Grovhacka mandlar och koriander.

5. Lägg upp grytan i djupa skålar och toppa med mandlar, koriander och yoghurt. Ät och njut!
