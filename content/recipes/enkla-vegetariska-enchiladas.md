---
title: Enkla vegetariska enchiladas
source: https://www.vegomagasinet.se/enkla-enchiladas#jump-content
servings: 4
---
4 portioner

## Ingredienser

- 6 stora tortillabröd
- 400 g sojafärs
- 1 gul lök, finhackad
- 1 påse tacokryddmix
- 2 dl krossade tomater
- 3 dl naturell yoghurt, t ex soja- eller havreyoghurt
- 1 burk majs (300-400 g)
- 1 burk (ca 2 dl) salsasås
- 2 dl riven ost, t ex mjölkfri

## Instruktioner

1. Sätt ugnen på 225 grader.
2. Stek sojafärsen i lite rapsolja.
3. Tillsätt löken och låt steka med ett tag. Rör ner tacokryddmixen, de krossade tomaterna, 1 dl av yoghurten samt majskornen. Låt allt bubbla ihop i några minuter.
4. Lägg sedan ut tortillabröden och fördela färsfyllningen på dem. Rulla ihop och lägg ner i en ugnsform.
5. Blanda ihop resterande yoghurt med salsasås och bred över tortillarullarna. Toppa med riven ost och gratinera sedan i ugnen i 10-15 minuter eller tills den börjar få fint med färg.
6. Servera med en god sallad och guacamole.
