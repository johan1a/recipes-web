---
title: Polsk saltgurkssoppa
source: https://zeinaskitchen.se/polsk-saltgurkssoppa/
servings: 4
---
4 portioner

## Ingredienser

- 6 st polska saltgurkor (ogorek kwaszony)
- 3-4 st potatisar
- 2 st morötter
- 2 st selleristjälkar
- 1 liten bit purjolök eller 2 vitlöksklyftor
- Ca 6 dl vatten
- 2 dl grädde
- 2 st grönsaksbuljongtärningar
- 2 st lagerblad
- 1 msk hackad färsk dill
- Salt & peppar

TIPS! Släng gärna i mer grönsaker i soppan. Alla sorters rotfrukter passar alldeles utmärkt att ha i.

## Instruktioner

Gör såhär:
1. Hacka alla grönsaker i önskad storlek och lägg i en gryta.
2. Smula över buljongtärningarna, tillsätt lagerblad, svartpeppar och häll sedan över hett vatten.
3. Vänta med att smaksätta med salt, saltgurkorna är ganska salta i smaken, smaka av saltmängden i slutet av koktiden.
4. Låt soppan koka under lock tills grönsakerna mjuknar, ca 15-20 min.
5. Riv saltgurkorna på grova sidan av rivjärnet.
6. Häll ner dom i grytan tillsammans med gurkspadet som bildas när du river dem.
7. Låt koka soppan koka i 10 min till.
8. Smaka av och häll eventuellt i mer gurkspad om du vill ha en syrligare smak.
9. Häll över grädde och dill. Låt soppan puttra i ca 3 min till. Smaka av och tillsätt eventuellt salt. Servera gärna soppan med en bit bröd. Smaklig spis!
