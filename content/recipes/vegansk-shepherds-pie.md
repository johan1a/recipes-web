---
title: Vegansk Shepherd's Pie
source: https://www.ica.se/recept/vegansk-shepherds-pie-726416/
servings: 6
---
6 portioner

## Ingredienser

### Potatismos

- 1 1/2 kg  mjölig potatis
- 75 g  mjölkfritt margarin
- 2 dl  havredryck
- salt
- svartpeppar

### Färs

- 1   stor gul lök
- 2 msk  finhackad färsk salvia
- 2 msk  olja (+ extra till formen)
- 1 1/2 tsk  torkad rosmarin
- 500 g  sojafärs
- 1/2 dl  tomatpuré
- 1   vitlöksklyfta
- 5 dl  vatten

## Instruktioner

1. Potatismos: Skala och skär potatisen i stora bitar. Koka mjuk i saltat vatten. Häll av vattnet och tillsätt margarinet. Mosa med en potatisstöt och tillsätt havredryck. Smaka av med salt och peppar.
2. Sätt ugnen på 225°C.
3. Färs: Hacka löken och salvian fint. Fräs löken i olja tillsammans med rosmarin och salvia tills löken är mjuk. Tillsätt färs och tomatpuré, pressa i vitlök och fräs ytterligare några minuter tills det får lite färg. Häll på vattnet och låt koka ihop några minuter. Smaka av med salt och peppar.
4. Häll färsen i en oljad ugnsform. Toppa med moset och ställ in i mitten av ugnen ca 20 minuter eller tills moset fått fin färg.
