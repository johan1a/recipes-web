---
title: Karamelliserad tofu med limeris
source: https://www.mathem.se/se/recipes/2096-mari-bergman-karamelliserad-tofu-med-limeris/?portions=4
servings: 4
---
## Ingredienser

**Limeris**
- 1 st Lime
- 2 dl Basmatiris
- 0,25 st Koriander - färsk

**Asiatisk coleslaw**
- 60 g Grönkål
- 1 st Lime
- 2 krm Chiliflakes
- 500 g Rödkål
- 2 krm Flingsalt
- 1 tsk Färsk ingefära
- 2 msk Rapsolja

**Karamelliserad tofu**
- 0,5 tsk Chiliflakes
- 400 g Tofu
- 3 msk Risvinäger
- 0,25 st Koriander - färsk
- 2 tsk Honung Flytande
- 3 msk Japansk soja
- 1 msk Färsk ingefära
- 1 msk Rapsolja
- 2 klyfta Vitlök Vanlig

## Instruktioner

- **Limeris:** Koka ris enligt anvisning på förpackningen.
- **Karamelliserad tofu:** Tärna tofun.
- Skala och riv ingefära och vitlök. Finhacka koriander. Blanda med olja, soja, risvinäger, honung och chiliflakes i en skål.
- **Asiatisk coleslaw:** Strimla rödkålen och blanda med grönkål i en skål.
- Skala och riv ingefära. Tillsätt ingefära, rivet limeskal, limesaft, olja, chiliflakes och salt. Blanda om ordentligt och ställ åt sidan till servering.
- **Karamelliserad tofu:** Hetta upp olja i en stekpanna och stek tofun på hög värme så att den får en krispig yta runt om.
- Tillsätt såsen i pannan och fräs ihop på hög värme under omrörning så att tofun karamelliseras. Stäng av värmen.
- **Limeris:** Tillsätt rivet limeskal, limesaft och finhackad koriander i riset.
- Servera tofun med limeris och asiatisk coleslaw. Ät och njut!
