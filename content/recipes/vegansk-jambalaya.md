---
title: Vegansk Jambalaya
source: https://www.eatingwell.com/recipe/277665/vegan-jambalaya/
servings: 6
---
4 portioner

## Ingredienser

- 1 msk rapsolja
- 170 gram vegansk rökt korv
- 4.8 dl hackad lök
- 2.4 dl hackad röd paprika
- 2.4 dl hackad selleri
- 2 medium jalapeños, med fröna borttagna och hackade
- 2.4 dl långkornigt vitt ris
- 4.8 dl buljong
- 1 burk (cirka 425 gram) krossade tomater
- 1/2 tsk svartpeppar
- 1/4 tsk salt
- 1.2 dl hackad salladslök

## Instruktioner

1. Hetta upp oljan i en stor stekpanna med medelhög värme. Tillsätt korv, och stek under omrörning tills den är brynt, 4 till 5 minuter.
2. Tillsätt lök, paprika, selleri och jalapeños; stek under omrörning då och då tills grönsakerna är mjuka, 5 till 6 minuter.
3. Tillsätt ris och rör om.
4. Tillsätt buljong, krossade tomater, peppar och salt. låt blandningen koka upp.
5. Sänk värmen till medel-låg. Täck med lock och låt småkoka tills riset mjuknat, ungefär 20 minuter.
6. Strö över salladslök.
