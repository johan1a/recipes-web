---
title: Tacopaj med formbar färs
source: https://javligtgott.se/recept/tacopaj-med-formbar-fars/
servings: 8
---
## Ingredienser

### Tacopaj
- 500 g Anammas Formbara färs
- 1 st Rödlök
- 2 msk Rökt paprikapulver
- 2 st Vitlöksklyfta
- 2 msk Torkad oregano
- 2 msk Mald spiskummin
- 1.5 msk Japansk sojasås
- 1.5 msk Tomatpuré
- 2 msk Rapsolja
- Svartpeppar - efter smak
- 1 st Färdig pajdeg (ca 230 g)

### Topping
- 200 g Växtbaserad mozzarella
- 2 dl Växtbaserad gräddfil
- 200 g Körsbärstomater
- 80 g Inlagda jalapeños
- 230 g Tacosås (valfri styrka)
- 50 g Nachos

## Instruktioner

### Tacopaj
- Ta fram färsen minst en timme innan och låt tina i rumstemperatur (eller i kylskåp över natten). Färsen ska vara mjuk men inte rumstempererad, annars tappar den sin bindförmåga.
- Tina pajdegen om den är fryst.
- Skala och hacka rödlök och vitlök.
- Blanda lök och vitlök med alla kryddor, rapsolja, tomatpuré och sojasås i den formbara färsen. Rör om och smaka av. Justera kryddningen efter smak.
- Kavla ut pajdegen om det behövs eller rulla ut den i en pajform (ca 22 cm i diameter) eller långpanna (30x40 cm).
- Stick hål i pajdegens botten för att ånga ska kunna släppas ut.
- Fördela färsblandningen jämnt över pajdegen.

### Topping
- Riv den växtbaserade mozzarellan och blanda med den växtbaserade gräddfilen. Ställ åt sidan.
- Skölj och skiva körsbärstomaterna.
- Krossa nachochipsen och sprid ett lager över tacopajen.
- Toppa därefter med tacosås, den växtbaserade ostblandningen, körsbärstomater och jalapeños.
- Grädda i ugnen på 200°C i ca 20 minuter, eller tills osten och grönsakerna fått fin färg.
- Om pajen inte får tillräckligt med färg, höj temperaturen till 225°C de sista 5 minuterna.
- Låt pajen vila i 5 minuter innan servering.
- Servera med extra jalapeños, nachos, tacosås och en fräsch sidosallad.
