
---
title: Rostad tomatsoppa med vitlöksbröd
source: https://www.koket.se/rostad-tomatsoppa-med-vitloksbrod
servings: 4
---

## Ingredienser

- 1 kg kvisttomat (romantica eller mindre plommontomater)
- 0,5 vitlök
- 1-2 rödlökar
- 2 msk olivolja
- 5 dl vatten
- 2 grönsaksbuljongtärningar
- 1-2 dl vispgrädde
- Salt
- Peppar

### Vitlöksbröd

- 0,5 baguette
- 50 g smör
- 1 klyfta vitlök
- 2 msk persilja, hackad

### Till servering

- Gräddfil
- Färsk basilika

## Instruktioner

### Rostad tomatsoppa

1. Sätt ugnen på 200 grader.
2. Skär tomaterna på hälften eller i kvartar. Skala och skär löken i mindre klyftor. Halvera vitlöken tvärs över på vitlökens "midja", behåll skalet på.
3. Lägg allt i en form och ringla olivolja över. Lägg i vitlöken med snittytan uppåt.
4. Sätt in i ugnen i 40-50 minuter tills tomaterna och vitlöken fått färg och blivit rostade.
5. Koka under tiden upp vatten och buljongtärningar i en kastrull.
6. Ta ut tomaterna ur ugnen. Pressa ut den rostade vitlöken ur skalet och släng bort skalet.
7. Lägg ned tomater, lök och vitlök i buljongen och låt koka ihop ett par minuter.
8. Mixa soppan slät med en stavmixer eller i blender.
9. Tillsätt grädden och fortsätt mixa soppan. Smaka av med salt och nymalen svartpeppar.

### Vitlöksbröd

1. Skär baguetten i skivor.
2. Smält smöret i en stekpanna. Tillsätt riven vitlök och hackad persilja.
3. Lägg i brödskivorna och stek dem gyllene på båda sidor. Salta lätt.
4. Servera soppan i skålar toppad med lite gräddfil och färsk basilika. Bjud med vitlöksbrödet.
