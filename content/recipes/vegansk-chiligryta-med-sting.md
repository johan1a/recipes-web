---
title: Vegansk chiligryta med sting
source: https://www.mathem.se/recept/vegansk-chiligryta-med-sting
servings: 4
---
## Ingredienser

### Vegansk chili
- 1 st Gul lök
- 2 st klyftor Vitlök Vanlig
- 1 st Röd Paprika
- 0.5 st Chilipeppar Röd
- 1 msk Rapsolja
- 2 msk Tomatpuré
- 600 g Krossade Tomater
- 2 tsk Spiskummin
- 1.5 tsk Paprikakrydda
- 1 tsk Cayennepeppar
- 1 tsk Strösocker
- 0.5 tsk Chiliflakes
- 1 tsk Salt Fint
- 2 krm Svartpeppar Malen
- 380 g Kidneybönor
- 190 g Kikärtor Kokta
- 340 g Majskorn
- 0.5 st krukor Koriander - färsk

### Tillbehör

- 4 portion Basmatiris
- 100 ml Havrefraiche
- 0.5 st krukor Koriander - färsk
- Tortillachips

## Instruktioner

- Tillbehör: Koka riset enligt anvisningarna på förpackningen.

- Vegansk chili: Skala och hacka gul lök och vitlök samt paprika och chilifrukt. Stek i olja på medelvärme under omrörning i några minuter. Tillsätt tomatpuré och fräs någon minut till.

- Tillsätt krossade tomater, spiskummin, paprikapulver, cayennepeppar, strösocker, chiliflakes, salt och svartpeppar.

- Tillsätt bönor, kikärtor och majs. Låt småputtra under lock i ca 15-20 min.

- Finhacka färsk koriander och rör ned i grytan.

- Tillbehör: Servera chilin med ris, vegansk crème fraîche, tortillachips och lite hackad färsk koriander.
