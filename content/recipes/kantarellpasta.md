---
title: Kantarellpasta
source: https://www.arla.se/recept/kantarellpasta/
servings: 4
---
## Ingredienser

- 1 st Schalottenlök
- 2 Vitlöksklyftor
- 1/2 Färsk röd chili
- 300g Färska kantareller
- 360g Pasta
- 25 Smör
- 1 ½ Torrt vitt vin
- 3dl Vispgrädde
- 1 tärning Svampbuljong
- Salt och svartpeppar
- 1 kruka Färsk persilja, hackad
- 2dl Parmesanost, nyriven

## Instruktioner

- Skala och finhacka lök och vitlök. Finhacka chilin. Ansa och dela svampen i mindre bitar.
- Koka pastan enligt anvisning på förpackningen.
- Hetta upp en stekpanna och fräs svmpen tills vätskan kokat in. Tillsätt smör, lök, vitlök och chili. Stek ytterligare 3-4 min.
- Tillsätt vin och låt det koka in. Häll på grädden och smula ner buljongen. Koka på svag värme ca 5 min. Smaka av med salt och peppar.
- Tillsätt lite pastavatten till svampen och blanda ner pastan. Strö över persilja och parmesan. Servera direkt.
