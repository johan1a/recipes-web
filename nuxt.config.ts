import { defineNuxtConfig } from "nuxt/config";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  build: {
    transpile: [
      "naive-ui",
      "vueuc",
      "@css-render/vue3-ssr",
    ],
  },
  modules: ["@nuxt/content"],
  target: "static",
  css: ["@/layouts/global.css"],
  head: {
    script: [],
  },
  plugins: ["@/plugins/naive-ui"],
});
